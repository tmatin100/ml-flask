# Overview

This is a simple project to elaborate on how to deploy a Machine Learning model using a Flask API. The original code can be found here https://github.com/MaajidKhan/DeployMLModel-Flask 

This project has four major parts:
1. model.py - This contains code for our Machine Learning model to predict employee salaries based on training data in 'hiring.csv' file.
2. app.py - This contains Flask APIs that receives employee details through GUI or API calls, computes the precited value based on our model and returns it.
3. template - This folder contains the HTML template (index.html) to allow user to enter employee detail and displays the predicted employee salary.
4. static - This folder contains the css folder with style.css file which has the styling required for out index.html file.

# Steps

   1) Create an app in Heroku
      1) Create an account for Heroku
      2) Create one app in Heroku
   2) Create a GitLab project and run the pipeline
      1) Create a GitLab account
      2) Clone this repository to a GitLab project
      3) Create GitLab Variables
      4) Run the pipeline
   3) Testing
      1) Connecting to the Flask application hosted by Heroku
      2) Input values for the Flask application

# How to deploy a Machine Learning Flask application in Heroku and GitLab

## 1. Create an app in Heroku
   1) Create an account for Heroku, if you don't already have one, at https://signup.heroku.com/
   2) Create one app in Heroku
      1) In the top right, click New > Create New App
      
      ![Image of Api Key Steps - 1](images/heroku_app_setup_1.png)

      2) Input a name for the app, then click Create App (You will need these names later). Repeat steps 1 and 2 two more times to create a test and production app

      ![Image of Api Key Steps - 2](images/heroku_app_setup_2.png)

## 2. Create a GitLab project and run the pipeline
   1) Create a GitLab account, if not done already, at https://gitlab.com/users/sign_up
   2) Clone this repository to a GitLab project
      1) On the GitLab home page, in the upper right, click New Project
      
      ![Image of GitLab Repo Setup Steps - 1](images/gitlab_new_proj_1.png)

      2) Select Import project

      ![Image of GitLab Repo Setup Steps - 2](images/gitlab_new_proj_2.png)

      3) Select Repo by URL and input https://gitlab.com/cmg_public/webinars/devops-crash-course/ml-flask.git for the Git repository URL, then click Create Project in the bottom left

      ![Image of GitLab Repo Setup Steps - 3](images/gitlab_new_proj_3.png)

   3) Create the following GitLab Variables under Settings > CI/CD > Variables:
         - HEROKU_API_KEY (The API Key that was created in Heroku)
            - In the top right, click your account icon > Account Settings
      
            ![Image of GitLab Variables Setup Steps - 1](images/heroku_api_key_1.png)

            - Scroll down and select Reveal for your API Key. Use this for the environment variable

            ![Image of GitLab Variables Setup Steps - 2](images/heroku_api_key_2.png)
            
         - HEROKU_APP (The name of the app that was created in Heroku)

         - Your Variables should look similar to the following:

         ![Image of GitLab Variables Setup Steps - 2](images/gitlab_variables_1.png)

   4) Run the pipeline
      1) Click CI/CD > Pipelines
      
            ![Image of Pipeline Startup Steps - 1](images/gitlab_start_pipeline_1.png)

      2) Click Run Pipeline on both screens, no changes need to be made at this point
      
            ![Image of Pipeline Startup Steps - 2](images/gitlab_start_pipeline_2.png)
      
            ![Image of Pipeline Startup Steps - 3](images/gitlab_start_pipeline_3.png)

## 3. Testing

   1) Connecting to the Flask application hosted by Heroku
      1) You should be able to easily connect to the application using the following format, replacing <Heroku_App_Name> with the name of your application in Heroku: 
      - https://<Heroku_App_Name>.herokuapp.com/
   2) Input values for the Flask application
      1) There are three inputs... Experience, Test Score, and Interview Score. You can put values in for any of these and click Predict to see the results!

# Summary
If you successfully setup the Machine Learning Application on Heroku, congratulations! If you are having issues, go back through the steps and make sure that everything is configured correctly. You should now have a fully functional CI/CD pipeline for the Machine Learning Application that you can further modify if desired.

# Next Steps
If you are interested in what Machine Learning can do, we highly recommend you do some practice yourself and deploy your own models!
